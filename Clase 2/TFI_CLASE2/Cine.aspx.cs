﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Cine : System.Web.UI.Page
{
    BLL.GESTOR_PELICULA gestor = new BLL.GESTOR_PELICULA();
    BLL.GESTOR_GENERO gestorG = new BLL.GESTOR_GENERO();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {          
            cbGenero.DataSource = gestorG.Listar();
            cbGenero.DataTextField = "Nombre";
            cbGenero.DataValueField= "id";
            cbGenero.DataBind();
            gestorG = null;
            Enlazar();
        }
    }

    void Enlazar()
    {
        Grilla.DataSource = gestor.Listar();
        Grilla.DataBind();
    }

    protected void btnNuevo_Click(object sender, EventArgs e)
    {
        txtNombre.Text = "";
        hID.Value = "0";
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(txtNombre.Text))
        {
            BE.PELICULA pelicula = new BE.PELICULA();

            pelicula.Id = int.Parse(hID.Value);
            pelicula.Pelicula = txtNombre.Text;

            pelicula.Genero = gestorG.Listar(int.Parse(cbGenero.SelectedValue));

            gestor.Grabar(pelicula);
            Enlazar();
            txtNombre.Text = "";
            hID.Value = "0";
        }
    }

    protected void Grilla_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        BE.PELICULA p = gestor.Listar(int.Parse(Grilla.Rows[int.Parse(e.CommandArgument.ToString())].Cells[2].Text));

        switch (e.CommandName)
        {
            case "btnSeleccionar":
                {
                    txtNombre.Text = p.Pelicula;
                    hID.Value = p.Id.ToString();

                    ListItem item = (from ListItem i in cbGenero.Items
                                     where i.Selected
                                     select i).First();
                    item.Selected = false;

                    item = (from ListItem i in cbGenero.Items
                            where i.Value == p.Genero.Id.ToString()
                            select i).First();
                    item.Selected = true;


                    break;
                }
            case "btnBorrar":
                {
                    gestor.Borrar(p);
                    txtNombre.Text = "";
                    hID.Value = "0";
                    Enlazar();
                    break;
                }
        }
    }
}