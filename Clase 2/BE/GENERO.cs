﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class GENERO
    {
        public GENERO()
        { }

        public GENERO(int i, string g)
        {
            this.id = i;
            this.nombre = g;
        }


        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


    }
}
