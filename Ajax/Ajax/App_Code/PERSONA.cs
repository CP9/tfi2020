﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de PERSONA
/// </summary>
public class PERSONA
{


    public static List<PERSONA> CrearPersonas()
    {
        List<PERSONA> personas = new List<PERSONA>();

        Random rnd = new Random();
        int cantidad = rnd.Next(1, 4000);

        for (int i = 0; i <cantidad; i++)
        {
            PERSONA p = new PERSONA();
            p.id = i;
            p.nombre = "PERSONA " + i.ToString();
            personas.Add(p);
        }

        return personas;
    }

    public PERSONA()
    {
        //
        // TODO: Agregar aquí la lógica del constructor
        //
    }

    private int id;

    public int ID
    {
        get { return id; }
        set { id = value; }
    }
    private string nombre;

    public string Nombre
    {
        get { return nombre; }
        set { nombre = value; }
    }




}