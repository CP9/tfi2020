﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de IDIOMA
/// </summary>
public class IDIOMA
{

    private string idioma;

    public string Idioma
    {
        get { return idioma; }
        set { idioma = value; }
    }


}