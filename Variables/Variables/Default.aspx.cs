﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            List<IDIOMA> idiomas = (List<IDIOMA>)Application["idiomas"];
            combo.DataSource = idiomas;
            combo.DataTextField = "idioma";

            combo.DataBind();
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Session["USUARIO"] = txtNombre.Text;

        Session.Add("Contraseña", txtPassword.Text);

        List<IDIOMA> idiomas = (List<IDIOMA>)Application["idiomas"];


        IDIOMA idioma = idiomas[combo.SelectedIndex];
        
        Session["IDIOMA"] = idioma; 

        Response.Redirect("Escritorio.aspx");
    }
}