﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IA___Algortimo_Movimiento_Fuerza_Bruta
{
    public partial class Form1 : Form
    {
        public int x;
        public int y;
        
        public Form1()
        {
            InitializeComponent();
        }

        void FuerzaBruta()
        {
            int X = x;

            if (pictureBox1.Location.X < x)
            {
                X = pictureBox1.Location.X + 1;
            }
            else if (pictureBox1.Location.X > x)
            {
                X = pictureBox1.Location.X - 1;
            }

            int Y = y;

            if (pictureBox1.Location.X < x)
            {
                X = pictureBox1.Location.X + 1;
            }
            else if (pictureBox1.Location.X > x)
            {
                X = pictureBox1.Location.X - 1;
            }

            pictureBox1.Location = new Point(X, Y);
        }

        void LineaVision()
        {
            float X = x;
            float Y = y;
            double dx, dy, mag;
            //Calculamos las diferencias
            dx = X-  pictureBox2.Location.X  ;
            dy = Y-  pictureBox2.Location.Y  ;

            //Buscamos la magnitud
            mag = Math.Sqrt((dx * dx) + (dy * dy));

            // Normalizar

            dx = dx / mag;
            dy /= mag;

            pictureBox2.Location = new Point(pictureBox2.Location.X + (int)Math.Round(dx,0), pictureBox2.Location.Y + (int)Math.Round(dy,0) );


        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            FuerzaBruta();
            LineaVision();
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            x = e.X;
            y = e.Y;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
