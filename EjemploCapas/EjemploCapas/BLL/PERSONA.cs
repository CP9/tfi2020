﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLL
{
    public class PERSONA
    {
        DAL.MP_PERSONA mp = new DAL.MP_PERSONA();


        public int Grabar(BE.PERSONA persona)
        {
            int res = 0;
            if (persona.Id == 0)
            {
                res = mp.Insertar(persona);
            }
            else
            {
                res = mp.Editar(persona);
            }
            return res;
        }

        public int Borrar(BE.PERSONA persona)
        {
            return mp.Borrar(persona);
        }

        public List<BE.PERSONA> Listar()
        {
            return mp.Listar();
        }

        public BE.PERSONA Listar(int id)
        {
            return mp.Listar(id);
        }
    }



}
