﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_PELICULA : MAPPER<BE.PELICULA>
    {
        public MP_PELICULA()
        {
            acceso = new ACCESO();
            accesopropio = true;
        }
        internal MP_PELICULA(ACCESO ac)
        {
            acceso = ac;
            accesopropio = false;
        }

        public override int Borrar(PELICULA objeto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", objeto.Id));
            Abrir();
            int resultado = acceso.Escribir("PELICULA_BORRAR", parametros);
            Cerrar();
            return resultado;
        }

        public override int Editar(PELICULA objeto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id", objeto.Id));
            parametros.Add(acceso.CrearParametro("@nom", objeto.Pelicula));
            parametros.Add(acceso.CrearParametro("@id_genero", objeto.Genero.Id));
            Abrir();
            int resultado = acceso.Escribir("PELICULA_EDITAR", parametros);
            Cerrar();
            return resultado;
        }

        public override int Insertar(PELICULA objeto)
        {
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@nom", objeto.Pelicula));
            parametros.Add(acceso.CrearParametro("@id_genero", objeto.Genero.Id));
            Abrir();
            int resultado = acceso.Escribir("PELICULA_INSERTAR", parametros);
            Cerrar();
            return resultado;
        }


        public  PELICULA Listar(int id)
        {
            Abrir();
            MP_GENERO mp_genero = new MP_GENERO(acceso);

            List<GENERO> generos = mp_genero.Listar();

            mp_genero = null;
            List<SqlParameter> parametros = new List<SqlParameter>();
            parametros.Add(acceso.CrearParametro("@id",id));

            DataTable tabla = acceso.Leer("PELICULA_LISTAR_id",parametros);
            Cerrar();

            DataRow registro = tabla.Rows[0];
            
            PELICULA p = new PELICULA();
            p.Id = int.Parse(registro["ID_PELICULA"].ToString());
            p.Pelicula = registro["nombre"].ToString();

            p.Genero = (from GENERO g in generos
                        where g.Id == int.Parse(registro["ID_GENERO"].ToString())
                        select g
                        ).First();
            return p;
        }



        public override List<PELICULA> Listar()
        {
            Abrir();
            MP_GENERO mp_genero = new MP_GENERO(acceso);

            List<GENERO> generos = mp_genero.Listar() ;
            List<PELICULA> peliculas = new List<PELICULA>();
            mp_genero = null;


            DataTable tabla = acceso.Leer("PELICULA_LISTAR");
            Cerrar();
            
            foreach (DataRow registro in tabla.Rows)
            {
                PELICULA p = new PELICULA();
                p.Id = int.Parse(registro["ID_PELICULA"].ToString());
                p.Pelicula = registro["nombre"].ToString();

                p.Genero = (from GENERO g in generos
                            where g.Id == int.Parse(registro["ID_GENERO"].ToString())
                            select g
                            ).First();

                peliculas.Add(p);
            }
            return peliculas;
        }
    }
}
