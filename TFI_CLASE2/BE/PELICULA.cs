﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class PELICULA
    {

        public PELICULA() { }

        public PELICULA(int i, string n, GENERO g)
        {
            id = i;
            pelicula = n;
            genero = g;
        }

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string pelicula;

        public string Pelicula
        {
            get { return pelicula; }
            set { pelicula = value; }
        }

        private GENERO genero;

        public GENERO Genero
        {
            get { return genero; }
            set { genero = value; }
        }
        public string Tipo
        {
            get {
                return genero.Nombre;
            }
        }

    }
}
